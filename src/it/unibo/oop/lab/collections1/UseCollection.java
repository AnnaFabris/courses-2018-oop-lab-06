package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	private final static int INIZIO = 1000;
	private final static int FINE = 2000;
	private static final int ELEMS = 1000000;
	private static final int TO_MS = 1000000;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        
    	/*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> list = new ArrayList<Integer>();
    	for(int i = INIZIO;i < FINE;i++) {
    		list.add(i);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	List<Integer> linkedList = new LinkedList<Integer>(list);
    	//linkedList.addAll(0, list);
    	
    	System.out.println(list);
    	System.out.println(linkedList);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int t = list.get(FINE - INIZIO - 1);
    	list.set(FINE - INIZIO - 1, list.get(0));
    	list.set(0, t);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	/*for(int elem : list) {
    		System.out.println(elem + " ");
    	}*/
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	
    	long time = System.nanoTime();
    	for(int i = 0;i < 100000;i++) {
    		list.add(0, i);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Adding " + ELEMS
                 + " in a arreyList took " + time
                 + "ns (" + time / TO_MS + "ms)");
    	
    	
        time = System.nanoTime();
    	for(int i = 0;i < 100000;i++) {
    		linkedList.add(0, i);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Adding " + ELEMS
                + " in a LinkedList took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        time = System.nanoTime();
    	for(int i = 0;i < 1000;i++) {
    		list.get(50000);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Reading " + ELEMS
                 + " in a arreyList took " + time
                 + "ns (" + time / TO_MS + "ms)");
    	
    	
        time = System.nanoTime();
    	for(int i = 0;i < 1000;i++) {
    		linkedList.get(50000);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Reading " + ELEMS
                + " in a LinkedList took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        final Map<String, Long> map = new HashMap<String, Long>();
        map.put("Africa", 1110635000L);
        map.put("Americas", 972005000L);
        map.put("Antarctica", 0L);
        map.put("Asia", 4298723000L);
        map.put("Europe", 742452000L);
        map.put("Oceania", 38304000L);
        		
        /*
         * 8) Compute the population of the world
         */
        long population = 0L;
        for(String elem: map.keySet()) {
        	population = population + map.get(elem).longValue();
        }
        
        System.out.println("Popolazione = " + population);
    }
}
